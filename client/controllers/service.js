(function () {
    
    angular.module("GroceryApp")
            .service("GroceryService", GroceryService);

    function GroceryService($http, $q) {
        var vm = this;

        vm.list = function (limit, offset) {
            var defer = $q.defer();
            $http.get("/api/grocerylist").then(function (result) {
                defer.resolve(result.data);
                consolet.log("where are you?",result.data);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

        vm.edit = function (id) {
            var defer = $q.defer();
            $http.get("/api/grocerylist/" + id)
                .then(function (result) {
                    defer.resolve(result.data);
                    console.log("here it is:" , stringify(result.data));
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };
        
        vm.save = function (product) {
            var defer = $q.defer();
            $http.put("/api/grocerylist/" + product.id, product)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.remove = function (product) {
            var defer = $q.defer();
            $http.delete("/api/grocerylist/" + product.id)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
        
        vm.search = function (searchType, keyword) {
            var defer = $q.defer();
            var params = {
                searchType: searchType,
                keyword: keyword
            };
            $http.get("/api/grocerylist", {
                params: params
                }).then(function (results) {
                console.log(results)
                    defer.resolve(results.data);
                }).catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
    }

    GroceryService.$inject = ['$http', '$q'];
    
})();