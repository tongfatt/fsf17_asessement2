var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");

const NODE_PORT = process.env.NODE_PORT || 8888; 

const CLIENT_FOLDER = path.join(__dirname + '/../client'); 
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages'); 

const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'sbz8459y'; 

const API_PRODUCTS_ENDPOINT = "/api/grocerylist"; 

var app = express();

//Creating a MySQL connection
var conn = new Sequelize(
    'grocerylist',           
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',         
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);


// establish database linkages and synchornization
var grocerylist = require('./models/grocerylist')(conn, Sequelize);




//middleware
app.use(express.static(CLIENT_FOLDER));     
app.use(bodyParser.json());     
app.use(bodyParser.urlencoded ({extended: false})) 


// set up http SQL calls connections configuration



// for listing products
 app.get(API_PRODUCTS_ENDPOINT, function(req, res){

var whereCondition = {};
var limit = req.query.limit || 20;
var offset = req.query.offset || 0;
var brand = '';
var name = '';

console.log(req.query.searchType);

 if((typeof req.query.searchType !== 'undefined')) {
        if(typeof req.query.searchType === 'string'){
            if(req.query.searchType=='Brand') {
                brand = req.query.keyword;
            }
            if(req.query.searchType=='Name') {
                name = req.query.keyword;
            }
        } else {
            brand = req.query.keyword;
            name = req.query.keyword;
        }
    }

    if(brand && name) {
        whereCondition = {
            where: {
                brand: {
                    $like: "%" + brand + "%"
                },
                name: {
                    $like: "%" + name + "%"
                }
            },
            limit: limit
        }
    } else {
        if(brand) {
            whereCondition = {
                where: {
                    brand: {
                        $like: "%" + brand + "%"
                    }
                },
                limit: limit
            }
        }
        else if(name) {
            whereCondition = {
                where: {
                    name: {
                        $like: "%" + name + "%"
                    }
                },
                limit: limit
            }
        }
        else {
            whereCondition = {
                limit: limit
            }
        }
    }

    grocerylist
        .findAll(whereCondition)
        .then(function (result) {
            if (result) {
                console.log(result);
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });

 });

 //for displaying based on product id
app.get(API_PRODUCTS_ENDPOINT+'/:id', function(req, res){

    grocerylist
        .findOne({
            where: {
                id: Number(req.params.id)
            }
        })
        .then(function(result){
            if (result) {
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });


});

// for inserting a new product

app.post(API_PRODUCTS_ENDPOINT, function(req,res){
    console.log("Inserting new product entry"); 
    console.log(req.body);
    tasks
        .create({
                id:req.body.id,
                upc12: req.body.upc12,
                brand: req.body.brand,
                name: req.body.name     
        }).then (function(tasks){      
            res.status(200).json(tasks);
        }).catch (function(err){
            res.status(500).json(err);
        })
});


// for updating

app.put(API_PRODUCTS_ENDPOINT+'/:id', function(req,res){
 var where = {};  
    where.id = req.params.id; 
 grocerylist
        .update({
              
                upc12: req.body.upc12,
                brand: req.body.brand,
                name: req.body.name
       
        } , {where: where} //the first where here is a sequelize key
        ).then (function(result){   //check using postman localhost:8080/api/departments/d012
            res.status(200)
                .json(result);
        }).catch(function(err){
            res.status(500)
                .json(err);
        })

});

// for delete
  app.delete(API_PRODUCTS_ENDPOINT+'/:id', function(req, res){
    var where = {};
    where.id = req.params.id;
    
    grocerylist
         .destroy({
            where: where
        })
        .then(function (result) {
            if (result == "1")
                res.json({success: true});
            else
                res.json({success: false});
        })
        .catch(function (err) {
            console.log("-- DELETE /api/grocerylist/:id catch(): \n" + JSON.stringify(err));
        });

  });


//exceptions cases handling

app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});