module.exports = function (conn, Sequelize) {

    var grocerylist = conn.define("grocerylist", {

        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        upc12: {
            type: Sequelize.INTEGER
        },
        brand: {
            type: Sequelize.STRING
        },
        name: {
            type: Sequelize.STRING
        }
    },{
        freezeTableName: true,
        timestamps:false
    });

    return grocerylist;

};